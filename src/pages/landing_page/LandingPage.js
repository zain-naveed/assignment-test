import React from "react";
import Navbar from "shared/component/navbar/navbar";
import styles from "./style.module.scss";
import classNames from "classnames";
import { Images } from "assets";
import TileItem from "shared/component/tile_item/TileItem";
import { planList, tileList } from "./Constant";
import ListItem from "shared/component/list_item/ListItem";
import CustomButton from "shared/component/custom_button/CustomButton";
import CustomInput from "shared/component/custom_input/CustomInput";
import Carousal from "shared/component/carousal/Carousal";
import Footer from "shared/component/footer/Footer";

export default function LandingPage() {
  return (
    <div className={classNames(styles.MainContainer)}>
      <Navbar />
      <section className={classNames(styles.HeaderContainer)}>
        <div className={classNames(styles.Content)}>
          <h2 className={classNames(styles.ContentHeading)}>
            Lorem ipsum <span>dolor</span> sit amet yo 👋
          </h2>
          <div className={classNames(styles.TileMainContainer)}>
            {tileList.map((tile, inx) => {
              return (
                <TileItem
                  key={`tile-item-${inx}`}
                  TileContainerStyle={classNames(styles.TileContainer)}
                  ImgStyle={styles.TileImg}
                  ContentStyle={styles.TileContent}
                  HeadingStyle={styles.TileHeading}
                  ParagraphStyle={styles.TileParagraph}
                  tileDetail={tile}
                />
              );
            })}
            <div className={classNames(styles.PurpleGradient)}></div>
          </div>
        </div>
        <div className={classNames(styles.HeaderImg)}>
          <img src={Images.header} alt="Image Not Found" />
        </div>
      </section>
      <div className={classNames(styles.CompanyAbout)}>
        {planList.map((plan, inx) => {
          return (
            <ListItem
              ContainerStyle={styles.Column}
              IconStyle={classNames(styles.Icon)}
              ContentStyle={classNames(styles.ListContent)}
              TitleStyle={classNames(styles.Title)}
              TextStyle={classNames(styles.Text)}
              itemDetail={plan}
              key={`plan-item-${inx}`}
            />
          );
        })}
      </div>
      <div className={classNames(styles.BrandContainer)}>
        <div className={classNames(styles.BrandContent)}>
          <h2 className={classNames(styles.BranHeading)}>Trusted by</h2>
          <p className={classNames(styles.BrandParagraph)}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vitae, in
            tristique senectus dui pharetra sit.
          </p>
        </div>
      </div>
      <Carousal />
      <div className={classNames(styles.FormMainContainer)}>
        <img
          src={Images.form}
          alt="Image Not Found!"
          className={classNames(styles.FormImage)}
        />
        <RegistrationForm />
      </div>
      <div className={classNames(styles.FootMain)}>
        <Footer />
        <small>Copyright&#169; 2013 i2c inc. All rights reserved.</small>
      </div>
    </div>
  );
}
const RegistrationForm = () => {
  const validateForm = () => {
    const name = document.getElementById("name").value;
    const comapny = document.getElementById("company").value;
    const email = document.getElementById("email").value;
    const nameError = document.getElementById("name-error");
    const emailError = document.getElementById("email-error");

    nameError.textContent = "";

    emailError.textContent = "";

    let isValid = true;

    if (name === "" || /\d/.test(name)) {
      nameError.textContent = "Please enter your name properly.";
      isValid = false;
    }

    if (email === "" || !email.includes("@")) {
      emailError.textContent = "Please enter a valid email address.";
      isValid = false;
    }
    if (name && email) {
      let getFormDetail = localStorage.getItem("form");
      if (getFormDetail) {
        alert("Your form is already submitted!");
      }
      if (!getFormDetail) {
        saveToStorage({ name, comapny, email });
        alert("Your form is successfully submitted!");
        resetForm();
      }
    }

    return isValid;
  };
  const saveToStorage = (formDetail) => {
    localStorage.setItem("form", JSON.stringify(formDetail));
  };
  const resetForm = () => {
    const name = document.getElementById("name");
    const comapny = document.getElementById("company");
    const email = document.getElementById("email");
    name.value = "";
    comapny.value = "";
    email.value = "";
  };
  return (
    <div className={classNames(styles.FormContainer)}>
      <h2 className={classNames(styles.FormHeading)}>Registration Form</h2>
      <p className={classNames(styles.FormParagraph)}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vitae, in
        tristique senectus dui pharetra sit.
      </p>
      <div>
        <CustomInput
          label={"Name"}
          type={"text"}
          inputId={"name"}
          placeholder={"Enter your name"}
          errorID={"name-error"}
          isRequire={true}
        />
        <CustomInput
          label={"Company"}
          inputId={"company"}
          type={"text"}
          placeholder={"Enter your company name"}
        />
        <CustomInput
          label={"Company"}
          type={"email"}
          inputId={"email"}
          errorID={"email-error"}
          placeholder={"Enter your email address"}
          isRequire={true}
        />
        <CustomButton
          Text={"Submit"}
          onClick={validateForm}
          ButtonStyle={classNames(styles.FormBtn)}
        />
      </div>
    </div>
  );
};
