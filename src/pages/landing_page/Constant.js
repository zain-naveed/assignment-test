import { Icons, Images } from "assets";

const tileList = [
  {
    name: "Andrew Schultz",
    image: Images.avatarOne,
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vitae, in tristique senectus dui pharetra sit.",
  },
  {
    name: "Andrew Schultz",
    image: Images.avatarTwo,
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vitae, in tristique senectus dui pharetra sit.",
  },
  {
    name: "Andrew Schultz",
    image: Images.avatarThree,
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vitae, in tristique senectus dui pharetra sit.",
  },
  {
    name: "",
    image: "",
    desc: "",
  },
];
const planList = [
  {
    id: 1,
    Icon: Icons.Lock,
    title: "24/7 Support",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  },
  {
    id: 2,
    Icon: Icons.Review,
    title: "1000+ reviews",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  },
  {
    id: 3,
    Icon: Icons.Cup,
    title: "And more",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  },
];
export { tileList, planList };
