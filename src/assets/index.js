// icons
import { ReactComponent as Logo } from "./icons/logo.svg";
import { ReactComponent as Cup } from "./icons/cup.svg";
import { ReactComponent as Lock } from "./icons/lock.svg";
import { ReactComponent as Review } from "./icons/reviews.svg";
import { ReactComponent as CocaCola } from "./icons/cola.svg";
import { ReactComponent as Microsoft } from "./icons/microsoft.svg";
import { ReactComponent as Twitter } from "./icons/twitter.svg";

// iamges
import header from "./images/header.png";
import avatarOne from "./images/avatar-one.png";
import avatarTwo from "./images/avatar-two.png";
import avatarThree from "./images/avatar-three.png";
import form from "./images/form.png";

const Icons = {
  Logo,
  Cup,
  Lock,
  Review,
  CocaCola,
  Microsoft,
  Twitter
};
const Images = {
  header,
  avatarOne,
  avatarTwo,
  avatarThree,
  form,
};

export { Icons, Images };
