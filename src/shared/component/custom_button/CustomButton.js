import React from "react";
import styles from "./style.module.scss";
import classNames from "classnames";

export default function CustomButton({ Text, ButtonStyle, onClick }) {
  return (
    <button
      onClick={onClick}
      className={classNames(styles.CutomButton, ButtonStyle && ButtonStyle)}
   
    >
      {Text}
    </button>
  );
}
