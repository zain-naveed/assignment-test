import React from "react";
import styles from "./style.module.scss";
// import ButtonComp from "custom_but";
import { Icons } from "assets";
import CustomButton from "shared/component/custom_button/CustomButton";

function Navbar() {
  return (
    <>
      <nav>
        <input type="checkbox" id={styles.Check} />
        <label for={styles.Check} className={styles.CheckBtn}>
          <i className="fas fa-bars"></i>
        </label>
        <label htmlFor="Logo" className={styles.logo}>
          <Icons.Logo />
        </label>
        <ul>
          <li>
            <a className={styles.active} href="wwww.google.com">
              About us
            </a>
          </li>
          <li>
            <a href="wwww.google.com">Registration</a>
          </li>
          <li>
            <a href="wwww.google.com">Careers</a>
          </li>
          <li>
            <CustomButton
              Text={"Contact Us"}
              ButtonStyle={styles.ButtonStyle}
            />
          </li>
        </ul>
      </nav>
    </>
  );
}

export default Navbar;
