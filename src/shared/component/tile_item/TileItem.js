import classNames from "classnames";
import React from "react";
import styles from "./style.module.scss";
import { Images } from "assets";
export default function TileItem({
  TileContainerStyle,
  ImgStyle,
  ContentStyle,
  HeadingStyle,
  ParagraphStyle,
  tileDetail,
}) {
  return (
    <div
      className={classNames(
        styles.TileContainer,
        TileContainerStyle && TileContainerStyle
      )}
    >
      {tileDetail.image && (
        <img
          src={tileDetail.image}
          className={classNames(ImgStyle && ImgStyle)}
          alt="Not Found"
        />
      )}
      <div
        className={classNames(styles.TileContent, ContentStyle && ContentStyle)}
      >
        <h3
          className={classNames(
            styles.TileHeading,
            HeadingStyle && HeadingStyle
          )}
        >
          {tileDetail.name}
        </h3>
        <p
          className={classNames(
            styles.TileParagraph,
            ParagraphStyle && ParagraphStyle
          )}
        >
          {tileDetail.desc}
        </p>
      </div>
    </div>
  );
}
