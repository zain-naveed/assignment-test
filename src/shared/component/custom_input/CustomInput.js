import classNames from "classnames";
import React from "react";
import styles from "./style.module.scss";
export default function CustomInput({
  label,
  isRequire,
  placeholder,
  value,
  onChange,
  type,
  error,
  errorID,
  inputId,
}) {
  return (
    <div className={classNames(styles.InputContainer)}>
      <label className={classNames(styles.InputLabel)}>
        {label}{" "}
        {isRequire && <span className={classNames(styles.IsRequire)}>*</span>}
      </label>
      <input
        className={classNames(styles.Input)}
        placeholder={placeholder}
        value={value}
        id={inputId}
        onChange={onChange}
        type={type}
      />
      {
        <div id={errorID} className={classNames(styles.Error)}>
          {error}
        </div>
      }
    </div>
  );
}
