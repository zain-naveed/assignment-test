import React from "react";
import styles from "./styles.module.scss";
import CustomButton from "shared/component/custom_button/CustomButton";
import { Icons } from "assets";
export default function Footer() {
  return (
    <>
      <footer>
        <input type="checkbox" id={styles.Check} />
        <label for={styles.Check} className={styles.CheckBtn}>
          <i className="fas fa-bars"></i>
        </label>
        <label htmlFor="Logo" className={styles.logo}>
          <Icons.Logo />
        </label>
        <ul className={styles.ListContainer}>
          <li>
            <a className={styles.active} href="wwww.google.com">
              FAQs
            </a>
          </li>
          <li>
            <a href="wwww.google.com">Privacy Policy</a>
          </li>
          <li>
            <a href="wwww.google.com">Other</a>
          </li>
          <li>
            <CustomButton
              Text={"Contact Us"}
              ButtonStyle={styles.ButtonStyle}
            />
          </li>
        </ul>
      </footer>
    </>
  );
}
