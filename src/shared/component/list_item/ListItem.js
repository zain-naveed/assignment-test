import React from "react";
import styles from "./style.module.scss";
import classNames from "classnames";

export default function ListItem({
  itemDetail,
  ContainerStyle,
  IconStyle,
  ContentStyle,
  TitleStyle,
  TextStyle,
}) {
  return (
    <div
      className={classNames(styles.Column, ContainerStyle && ContainerStyle)}
    >
      <itemDetail.Icon
        className={classNames(styles.Icon, IconStyle && IconStyle)}
      />
      <div
        className={classNames(
          styles.ListContent && ContentStyle && ContentStyle
        )}
      >
        <div className={classNames(styles.Title, TitleStyle && TitleStyle)}>
          {itemDetail.title}
        </div>
        <div className={classNames(styles.Text, TextStyle && TextStyle)}>
          {itemDetail.text}
        </div>
      </div>
    </div>
  );
}
