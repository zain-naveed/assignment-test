import React from "react";
import Slider from "react-slick";
import styles from "./style.module.scss";
import { Icons } from "assets";
import classNames from "classnames";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

export default function Carousal() {
  const settings = {
    slidesToShow: 3,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };
  return (
    <div className={classNames(styles.Container)}>
      <Slider {...settings}>
        <div className={classNames(styles.CarouselItem)}>
          <Icons.Microsoft className={classNames(styles.Icon)} />
        </div>
        <div className={classNames(styles.CarouselItem)}>
          <Icons.Twitter className={classNames(styles.Twitter)} />
        </div>
        <div className={classNames(styles.CarouselItem)}>
          <Icons.CocaCola className={classNames(styles.Icon)} />
        </div>
      </Slider>
    </div>
  );
}
